<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    $country = \App\Models\Country::all();
    return view('index',[
        'country' => $country
    ]);
});

Route::get('/user-form/{data}/user_id/{user_id}/key_value/{value}','App\Http\Controllers\HomeController@store');
Route::get('/send-email-to-user/{id}','App\Http\Controllers\HomeController@sendmailToUser');
