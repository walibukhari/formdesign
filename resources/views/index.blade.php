<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
<meta name="viewport" content="width=device-width, height=device-height, initial-scale=1, maximum-scale=1, minimum-scale=1, user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Slideform</title>
    <link href="https://fonts.googleapis.com/css?family=Raleway:400,600,700" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('slideForm/docs/css/slideform.css')}}">

    <style>


        form {
            opacity: 0;
            transition: all .3s ease;
        }

        form.slideform-form {
            opacity: 1;
        }

        html, body {
            display: flex;
            align-items: center;
            justify-content: center;
            flex-direction: column;
            background-size: cover;
            font-family: inherit;
            background-position: center top;
            background-repeat:no-repeat;
            background-image: url("./large.png");
            height: 100%;
            width: 100%;
            padding: 0;
            margin: 0;
            overflow: hidden;
            /*position: fixed;*/
            /*top: 0;*/
            /*bottom: 0;*/
            /*left: 0;*/
            /*right: 0;*/
        }

        body, h1, h2, h3, p, button, input, select, textarea {
            font-family: 'Raleway', Arial, sans-serif;
        }

        input {
            font-size: 16px;
        }

        label {
            color: #fff;
            text-align:left;
            text-transform: uppercase;
            display: block;
            font-size: 20px;
            margin-bottom: 20px;
            letter-spacing: .5px;
            font-weight: 100;
        }

        h1 {
            letter-spacing: .5px;
        }

        h2, h3 {
            margin-top: 0;
        }

        h2 {
            font-size: 28px;
            margin: 0 0 15px;
        }

        h3 {
            font-size: 20px;
            line-height: 1.4em;
            font-weight: bold;
            margin-bottom: 20px;
            margin-top: 20px;
        }

        h4 {
            font-size: 16px;
            font-weight: bold;
            margin: 20px 0 10px;
        }

        p {
            font-size: 16px;
            line-height: 1.6em;
            margin-bottom: 15px;
        }

        a {
            -webkit-tap-highlight-color: transparent;
        }


        .gist .gist-file {
            margin-top: 40px;
        }

        .gist .gist-meta {
            display: none !important;
        }

    </style>
    <style>
        form.slideform-form input:not([type=checkbox]):not([type=radio]):not([type=submit]):focus, form.slideform-form textarea:focus{
            border-bottom: 2px solid #fff;
        }
        .select2-container--default .select2-selection--single .select2-selection__arrow b {
            border-color: #fff transparent transparent transparent !important;
            border-style: solid;
            border-width: 5px 4px 0 4px;
            height: 0;
            left: 50%;
            margin-left: -4px;
            margin-top: -2px;
            position: absolute;
            top: 50%;
            width: 0;
        }
        form.slideform-form input:not([type=checkbox]):not([type=radio]):not([type=submit]), form.slideform-form textarea{
            border-bottom: 1px solid #fff;
        }
        #emailAddress:focus{
            border-bottom: 2 solid #fff;
        }
        #phoneNumber{
            width:55%;
        }
        #emailAddress:active{
            border-bottom: 2px solid #fff;
        }
        #emailAddress:visited{
            border-bottom: 2px solid #fff;
        }
        form.slideform-form input:not([type=checkbox]):not([type=radio]):not([type=submit]), form.slideform-form textarea {
            width: 97%;
            transition: box-shadow 0.1s ease-out 0s;
            box-shadow: rgba(255, 255, 255, 0.3) 0px 1px;
            padding: 15px;
            color: #fff;
            font-size: 20px;
            box-sizing: border-box;
            outline: none;
            border-radius: 0px;
            -webkit-appearance: none;
            -moz-appearance: none;
            appearance: none;
            background: transparent;
            border-top: 0px;
            border-left: 0px;
            border-right: 0px;
        }
        .image-logo{
            display: flex;
            justify-content: flex-end;
            height:100%;
            flex-direction: column;
            align-items: center;
            margin: 0 auto;
            width: 30%;
            max-height: 55%;
            min-height: 55%;
        }
        .customPstyle{
            width: 800px;
            text-align: center;
            color: #fff;
            font-size: 21px;
        }
        form.slideform-form .slideform-slide.active {
            opacity: 1;
            align-items: center;
        }
        form.slideform-form .slideform-group{
            width: 50%;
            max-width:unset;

        }

        .sliderFormStart0:first-child{
            z-index:9999;
            font-weight: bold !important;
            font-size: 25px;
            padding: 10px 25px !important;
        }

        form.slideform-form .slideform-group{
            position:inherit;
        }
        ::placeholder { /* Chrome, Firefox, Opera, Safari 10.1+ */
            color: #fff;
            font-size: 25px;
            opacity: 1; /* Firefox */
            padding-left: 0px;
        }
        .sliderFormStart0:nth-child(2) {
            position: absolute !important;
            top:0px;
            display: none;
        }
        .fa-arrow-right{
            font-size: 12px !important;
            margin-right: 7px;
            margin-left: 3px;
        }
        
        .fa-arrow-left{
            color: #fff;
            font-size: 16px !important;
            margin-right: 0px;
            margin-left: 0px;
            position: absolute;
            margin-top: 10px;
            z-index: 99999;
        }
        .fa-asterisk{
            position: relative;
            font-size: 10px !important;
            bottom: 11px;
        }
        input[type=text] {
            margin-left: 33px;
        }
        .sliderFormStart0{
            margin-left: 33px !important;
        }
        .sliderFormStart2{
            margin-left: 0px !important;
        }
        .slideform-btn-submit{
            margin-left: 33px !important;
        }
        input[type=email] {
            margin-left: 33px;
        }

        form.slideform-form .slideform-footer{
            background: transparent;
            padding: 0 15px;
            border-top: 0px solid #f6f6f6;
            z-index: 1000;
            display: flex;
            align-items: center;
            height: 50px;
            justify-content: flex-end;
        }
        form.slideform-form .slideform-progress-bar{
            height: 5px;
            border-radius: 5px;
            max-width: 200px;
            background: #f6f6f6;
            display: inline-block;
            overflow: hidden;
            line-height: 0;
            flex: 1;
            margin-right: 20px;
            width: 100%;
        }
        .box{
            max-width: 200px;
            background: #5555;
            width: 100%;
            padding: 5px 16px;
            border-radius: 14px;
            position: relative;
            bottom: 3px;
            right: 12px;
        }
        form.slideform-form .slideform-progress-bar span{
            display: inline-block;
            width: 0%;
            height: 100%;
            background: #555;
            transition: all 0.5s ease;
        }
        .firstChild0 {
            position: absolute;
            width: inherit;
            top: 0;
            text-align: center;
        }
        @media (max-width:991px) and (min-width:320px) {
            form.slideform-form .slideform-group {
                /* padding-top: 0px; */
                width: 100%;
                max-width: unset;
                display: flex;
                flex-direction: column;
                align-items: baseline;
                justify-content: center;
                height: 100%;
            }
            .custom-ul {
                border-bottom: dashed;
                border-bottom-color: #fff;
                padding: 0px !important;
                margin-left: 0px !important;
            }
            input[type=email] {
                margin-left: 0px;
            }
            input[type=text] {
                margin-left: 0px;
            }
            .customPstyle {
                width: 100%;
                text-align: center;
                color: #fff;
                font-size: 21px;
            }
            form.slideform-form .slideform-group {
                width: 100%;
                max-width: unset;
            }
            .customButtons{
                display:flex;
            }
            .sliderFormStart0 {
                margin-left: 0px !important;
            }
            .slideform-btn-submit {
                margin-left: 0px !important;
            }
            .firstChild0{
                position: absolute;
                top: 0px;
                /*position: fixed !important;*/
                width: 100%;
                text-align: center;
                bottom: 25px;
                /*background-color: rgba(0, 0, 0, 0.05);*/
                /*box-shadow: rgba(0, 0, 0, 0.1) 0px -1px;*/
                height:85px;
                /*margin-top: 130px;*/
            }
        }

        @media (max-width:500px) {
            .image-logo {
                width: 100% !important;
                display: flex;
                justify-content: center;
                margin-top: 35px;
                flex-direction: column;
                align-items: center;
            }
            html, body {
                font-family: inherit;
                background-position: center;
                background-repeat:no-repeat;
                background-image: url("./large.png");
                height: 100%;
                width: 100%;
                padding: 0;
                margin: 0;
                overflow: hidden;
                position: fixed;
                top: 0;
                left: 0;
                /*left: 0;*/
                /*right: 0;*/
            }
            .firstChild0{
                position: fixed !important;
                width: 90%;
                top:unset;
                text-align: center;
                /*bottom: 25px;*/
                background-color: rgba(0, 0, 0, 0.05);
                box-shadow: rgba(0, 0, 0, 0.1) 0px -1px;
                height:85px;
                margin-top: 0px;
            }
            .image-logo > img {
                width:90%;
            }
        }
        @media (max-width:375px) {
            form.slideform-form .slideform-group {
                padding-top: 175 !important;
                width: 100%;
                max-width: unset;
            }
            .firstChild0 {
                position: fixed !important;
                width: 90%;
                top: unset;
                text-align: center;
                /* bottom: 25px; */
                background-color: rgba(0, 0, 0, 0.05);
                box-shadow: rgba(0, 0, 0, 0.1) 0px -1px;
                height: 85px;
                margin-top: 0px;

            }
            .image-logo {
                display: flex;
                justify-content: center;
                margin-top: 20px;
                flex-direction: column;
                align-items: center;
                width: 100% !important;
            }
        }
        @media (max-width:320px) {
            form.slideform-form .slideform-group {
                padding-top: 174px !important;
                width: 100%;
                max-width: unset;
            }
        }
        .boldButton{
            font-weight: bold !important;
            font-size: 23px;
        }
    </style>
    <style>
        .custom-ulli{
            list-style: none;
            background: #74CCE4;
            padding: 12px;
            margin-top: 4px;
            border-radius: 6px;
            border-radius: 6px;
            border: 1px solid #fff;
            color: #fff;
            font-size: 25px;
            cursor: pointer;
        }
        .custom-ul{
            border-bottom: dashed;
            border-bottom-color: #fff;
            padding: 0px !important;
            margin-left: 33px;
        }
        .select2-container{
            width: 70px !important;
        }
        .select2-container .select2-selection--single{
            display: flex;
            background-color: #fff;
            border: 1px solid #aaa;
            border-radius: 4px;
            height: 50px !important;
            align-items: center;
        }
        .select2-container--default .select2-selection--single .select2-selection__rendered{
            line-height: 0px !important;
        }
        .select2-container .select2-selection--single .select2-selection__rendered {
            display: block;
            padding-left: 14px !important;
            padding-right: 20px;
            overflow: hidden;
            text-overflow: ellipsis;
            white-space: nowrap;
        }
        .select2-container--open .select2-dropdown--below {
            border-top: none;
            border-top-left-radius: 0;
            border-top-right-radius: 0;
            width: 200px !important;
        }
        .select2-container--default .select2-selection--single .select2-selection__arrow {
            height: 26px;
            display: flex;
            align-items: center;
            position: absolute;
            top: 13px !important;
            right: -5px !important;
            width: 20px;
        }
        .select2-container--default .select2-selection--single {
            display: flex !important;
            border-top:0px !important;
            border-left:0px !important;
            border-right:0px !important;
            align-items: center !important;
            background-color: transparent !important;
            border-bottom: 1px solid #efefef !important;
            border-radius: 4px;
        }
        .select2-container--open .select2-dropdown--below {
            border-top: none;
            border-top-left-radius: 0;
            border-top-right-radius: 0;
            width: 286px !important;
        }
        @media (max-width: 575px) {
            #phoneNumber {
                width: 55%;
                display: flex;
                position: absolute;
                right: 50px;
            }
            .newPage{
                height: 100%;
                background: #4DAFD6;
                padding: 20px;
                padding-top:20px;
            }
            .customStSet{
                flex-direction: row;
            }
        }
    </style>
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" />
</head>
<body>
<div class="image-logo">
    <img src="{{asset('/default.png')}}" />
    <p class="customPstyle">
        Bitte unser Formular aus. Einer Unserer Mitarbeiter wird sich innerhalb von 24h bei Dir melden und sich deinem Anliegen annehmen!
    </p>
</div>
<form action="">

    <div class="slideform-slide">
        <div class="slideform-group">
        </div>
    </div>

    <div class="newPage" style="display: none;">
        <label for="input">
            <small id="titleSection" style="display: none;color:red;">Anrede Required</small>
        </label>
        <i class="fa fa-arrow-left" id="faArrowClick" aria-hidden="true"></i>
        <input type="text" onkeyup="oncChangeFunction()" style="padding-left:30px;" id="title2" autofocus="autofocus" name="input" placeholder="Write Anrede here ...">
        <div class="custom-drop-down-box" id="custom-drop-down-box2">
            <ul class="custom-ul" id="custom-ul2">
                <li class="custom-ulli ">Herr</li>
                <li class="custom-ulli ">Frau</li>
                <li class="custom-ulli ">Andere</li>
            </ul>
        </div>
    </div>
    
    <div class="slideform-slide">
        <div class="slideform-group">
            <label for="input">1<i class="fa fa-arrow-right" aria-hidden="true" ></i>Email <i class="fa fa-asterisk" aria-hidden="true"></i>
                <small id="emailSection"  style="display: none;color:red;">Email Required</small>
            </label>
            <input type="email" id="emailAddress" autofocus="autofocus" name="input" placeholder="Write Email here ...">
            <input type="text" style="display: none;" id="userId" name="input" />
        </div>
    </div>

    <div class="slideform-slide">
        <div class="slideform-group">
            <label for="input">2<i class="fa fa-arrow-right" aria-hidden="true"></i> Anrede <i class="fa fa-asterisk" aria-hidden="true"></i>
                <small id="titleSection" style="display: none;color:red;">Anrede Required</small>
            </label>
            <input type="text" id="title3" style="display: none;" autofocus="autofocus" name="input" placeholder="Write Anrede here ...">
            <input type="text" onkeyup="oncChangeFunction()" id="title" autofocus="autofocus" name="input" placeholder="Write Anrede here ...">
            <div class="custom-drop-down-box" id="custom-drop-down-box" style="display: none;">
                <ul class="custom-ul" id="custom-ul">
                    <li class="custom-ulli ">Herr</li>
                    <li class="custom-ulli ">Frau</li>
                    <li class="custom-ulli ">Andere</li>
                </ul>
            </div>
        </div>
    </div>


    <div class="slideform-slide">
        <div class="slideform-group">
            <label for="input">3<i class="fa fa-arrow-right" aria-hidden="true"></i> Vorname <i class="fa fa-asterisk" aria-hidden="true"></i>
                <small id="firstNameSection" style="display: none;color:red;">Vorname Required</small>
            </label>
            <input type="text" id="firstName" name="input" autofocus="autofocus" placeholder="Write Vorname here ...">
        </div>
    </div>



    <div class="slideform-slide">
        <div class="slideform-group">
            <label for="input">4<i class="fa fa-arrow-right" aria-hidden="true"></i> Nachname <i class="fa fa-asterisk" aria-hidden="true"></i>
                <small id="lastNameSection" style="display: none;color:red;">Nachname Required</small>
            </label>
            <input type="text" id="lastName" name="input" autofocus="autofocus" placeholder="Write Nachname here ...">
        </div>
    </div>


    <div class="slideform-slide">
        <div class="slideform-group">
            <label for="input">5<i class="fa fa-arrow-right" aria-hidden="true"></i> Telefon Nr <i class="fa fa-asterisk" aria-hidden="true"></i>
                <small id="phoneNumberSection" style="display: none;color:red;">Telefon Nr Required</small>
            </label>
            <div class="customStSet" style="display: flex;">
                <div style="width: 10%">
                    <div>
                        <select id="cmbIdioma" autofocus="autofocus" style="width: 50px;">
                            <option value="Germany" selected>Germany</option>
                            @foreach($country as $count)
                                <option value="{{$count->countryname}}">{{$count->countryname}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div style="width: 90%">
                    <input type="text" id="phoneNumber" autofocus="autofocus" name="input" placeholder="Write Telefon Nr here ...">
                </div>
            </div>
        </div>
    </div>

    <div class="slideform-slide">
        <div class="slideform-group">
            <label for="input">6<i class="fa fa-arrow-right" aria-hidden="true"></i> Was ist Dein Business <i class="fa fa-asterisk" aria-hidden="true"></i>
                <small id="businessSection" style="display: none;color:red;">Was ist Dein Business Required</small>
            </label>
            <input type="text" id="business" name="input" autofocus="autofocus" placeholder="Write Was ist Dein Business here ...">
        </div>
    </div>

    <footer class="slideform-footer">
        <div class="box">
            <span id="countTotal">0/0</span>
            <div class="slideform-progress-bar">
                <span></span>
            </div>
        </div>

        <div class="buttons customButtons">
            <button class="slideform-btn slideform-btn-next"><i class="icon-chevron-down"></i></button>
            <button class="slideform-btn slideform-btn-prev"><i class="icon-chevron-up"></i></button>
        </div>
    </footer>

</form>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet"/>
<script src="https://cdnjs.cloudflare.com/ajax/libs/fastclick/1.0.6/fastclick.min.js"></script>
<script>
    $(document).ready(function() {
       $('input').on('focus', function(e) {
            e.preventDefault();
            e.stopPropagation();
        	setTimeout(
        		function(){
        			windows.scrollTo(0, 0);
        		}, 100);
        }); 
    });
    function find(arr,val) {
        var result = [];
        for (var i in arr) {
            if (arr[i].match(val)) {
                result.push(arr[i]);
            }
        }
        return result;
    }
    function oncChangeFunction() {
        console.log("$(this).val()");
        let val = '';
        if (window.matchMedia('(max-width: 575px)').matches) {
            console.log('this is 575px');
            console.log($('#title2').val());
            val = $('#title2').val();
        } else {
            console.log($('#title').val());
            val = $('#title').val();
        }
        let a = 'Herr';
        let a1 = 'herr';
        let b = 'Frau';
        let b1 = 'frau';
        let c = 'Andere';
        let c1 = 'andere';
        let arr = [a,b,c,a1,b1,c1];
        let data = find(arr,val);
        console.log('data checkinggg....');
        if (window.matchMedia('(max-width: 575px)').matches) {
            console.log('this is 575 data results');
            console.log(data)
            if (data.length == 1) {
                let html = ' <li class="custom-ulli ">' + data[0] + '</li>';
                $('#custom-ul2').append('');
                $('#custom-ul2').html('');
                $('#custom-ul2').append(html);
            } else {
                let html = '<li class="custom-ulli">Herr</li>\n' +
                    '                    <li class="custom-ulli ">Frau</li>\n' +
                    '                    <li class="custom-ulli ">Andere</li>';
                $('#custom-ul2').append('');
                $('#custom-ul2').html('');
                $('#custom-ul2').append(html);
            }
        } else {
            if (data.length == 1) {
                let html = ' <li class="custom-ulli">' + data[0] + '</li>';
                $('#custom-ul').append('');
                $('#custom-ul').html('');
                $('#custom-ul').append(html);
            } else {
                let html = '<li class="custom-ulli ">Herr</li>\n' +
                    '                    <li class="custom-ulli ">Frau</li>\n' +
                    '                    <li class="custom-ulli ">Andere</li>';
                $('#custom-ul').append('');
                $('#custom-ul').html('');
                $('#custom-ul').append(html);
            }
        }
    }

    $("#cmbIdioma").select2({
        templateResult: function (idioma) {
            var $span = $("<span><img src='https://www.free-country-flags.com/countries/"+idioma.id+"/1/tiny/" + idioma.id + ".png'/> " + idioma.text + "</span>");
            return $span;
        },
        templateSelection: function (idioma) {
            if(idioma.text == "Germany") {
                var $span = $("<span>" + "<img  style='width: 37px;\n" +
                    "    height: 24px;\n" +
                    "    border-radius: 3px;' src='{{asset('germany-flag-image-free-download.jpg')}}'/>" + "</span>");
            } else {
                var $span = $("<span>" + "<img  style='width: 37px;\n" +
                    "    height: 24px;\n" +
                    "    border-radius: 3px;' src='https://www.free-country-flags.com/countries/" + idioma.id + "/1/tiny/" + idioma.id + ".png'/>" + "</span>");
            }
            return $span;
        }
    });
</script>
<script
    src="https://code.jquery.com/jquery-3.3.1.min.js"
    integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
    crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/additional-methods.min.js"></script>
<script src="{{asset('slideForm/docs/js/slideform.js')}}"></script>
<script>
window.addEventListener("load", function() { window. scrollTo(0, 0); });
    ( function ($) {
        jQuery.validator.addMethod("equals", function(value, element, param) {
            return this.optional(element) || value === param;
        });

        $(document).ready( function () {
            var $form = $('form');

            $form.slideform({
                validate: {
                    rules: {
                        group1: {
                            required: true,
                            equals: "valid"
                        },
                        group7: {
                            required: true,
                            equals: "valid"
                        }
                    },
                    messages: {
                        group7: {
                            required: 'Please select an option',
                            equals: 'You must select valid!'
                        },
                        group1: {
                            required: 'Please Enter Email Address',
                            equals: 'You must select valid!'
                        }
                    }
                },
                submit: function (event, form) {
                    let userId = $('#userId').val();
                    let url = '/send-email-to-user/'+userId
                    $.ajax({
                        url:url,
                        method:'GET',
                        success: function (response) {
                            console.log('response');
                            console.log(response);
                            setTimeout(function () {
                                window.location.href='https://danke.social-crow.de/'
                            },500);
                        },
                        error: function (error) {
                            console.log('error');
                            console.log(error);
                            setTimeout(function () {
                                window.location.href='https://danke.social-crow.de/'
                            },500);
                        }
                    });
                    $form.trigger('goForward');
                }
            });
        });
    }(jQuery))
</script>
</body>
</html>
