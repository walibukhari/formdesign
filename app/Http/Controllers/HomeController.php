<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Mailgun\Mailgun;

class HomeController extends Controller
{
    public function store($data,$user_id,$key_value) {
        $check = User::where('email','=',$data)->first();
        if($user_id == 0) {
            if(!$check) {
                $user = User::create([
                    'email' => $data
                ]);
                return collect([
                    'status' => true,
                    'data' => $user->id
                ]);
            } else {
                return collect([
                    'status' => true,
                    'data' => $check->id
                ]);
            }
        } else {
            if($check) {
                User::where('email','=',$check)->update([
                    $key_value => $data
                ]);
                return collect([
                    'status' => true,
                    'data' => $check->id
                ]);
            } else {
                User::where('id','=',$user_id)->update([
                    $key_value => $data
                ]);
                return collect([
                    'status' => true,
                    'data' => $user_id
                ]);
            }
        }
    }

    public function sendmailToUser($id){
        $user = User::where('id','=',$id)->first();
        $senderEmail = 'info@social-crow.de';
        $senderName  ='Social-Crow';
        $email = $user->email;

        Mail::send('mails.sendEmail', array('user' => $user), function ($message) use ($senderEmail, $senderName, $email) {
            $message->from($senderEmail, $senderName);
            $message->to($email)
                ->subject('Success Email');
        });
    }
}
